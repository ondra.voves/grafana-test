# Grafana test

Simple docker-compose stack with Nginx/telegraf/influxdb/grafana

# Run

```bash
$ docker-compose up
```

# Grafana
- URL: `http://localhost:3000`
- User: `admin`
- Password: `admin`

# Influxdb (datasource)

- URL: `http://influxdb:8086`
- User: `telegraf`
- Password: `telegraf`
- db: `telegraf`

# Sample dashboard

- Sample dashboard is in dir `dashboards`
- Can be imported to grafana (dashboard->import and upload json)